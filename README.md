# Side-Scroller Template #

An ActionScript 3 template for prototyping platformer/side-scroller games. The template uses the [Flixel](http://flixel.org/) game framework, the [Pixelpracht Flash TMX Parser](http://pixelpracht.wordpress.com/2010/03/31/flash-tmx-parser/) library for [Tiled](http://www.mapeditor.org/) map loading, and the [MinimalComps](http://www.minimalcomps.com/) library for the editor GUI, which allows a few variables, such as movement speed, to be tweaked in real-time.

[Try it out](http://christopherdcanfield.com/projects/ludumdare/30/templates/platformer-side-scroller/)

![side-scroller.png](https://bitbucket.org/repo/zABgnA/images/1199317461-side-scroller.png)

Christopher D. Canfield  
September 2014